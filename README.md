# TTL-Mantis: database construction and processing new data

## overview

A [Eureka](https://www.waterprobes.com/multi-probes-sondes-water-quality-monitoring) [Manta+35](https://www.waterprobes.com/product-page/manta-35) *in situ* data sonde to measure water quality, including optical dissolved organic carbon characteristics, at high temporal resolution was installed in the Tempe Town Lake (Tempe, Arizona, USA) in 2018. The sensor is a part of the portfolio of CAP LTER projects building long-term datasets around water quality and other key ecological indicators in the greater Phoenix metropolitan area and surrounding Sonoran desert region. The sensor supersedes an earlier project in which twice-weekly samples were collected from the lake for analysis of a wide suite of water-quality parameters, and these collections were performed simultaneously for a given period to facilitate sensor and model calibration.

This repository houses workflows regarding:

1. initial development of a PostgreSQL database to house data sonde readings and supporting information, and 
2. processing new data


## database construction

The data sonde produces a single, flat file of events (time) * measured parameters and diagnostics. The straightforward database model splits the data into a separate events and a normalized measurements tables with a lookup table to facilitate parameter details. An additional table houses maintenance log entries.

![](assets/figures/ttl_mantis_schema.png)


## processing new data

The workflow entails running a bash cleaning script (`mantisPreprocess.sh`) included in this repository on each input file (could be done in R but bash is just easier):

```sh chmod +x mantisPreprocess.sh```  
```sh find . -name "*.csv" -exec ./mantisPreprocess.sh {} \;```

Uploading is a two-step process focusing on (1) events, then (2) measurements/observations. Though each step is wrapped in a transaction, a problem at step 2 would not be encountered until after successful application of step 1. Each upload should be run first on a development database to help identify any potential data untangling issues.

After the initial cleaning step, the remaining workflow of importing, formatting, and validating the data is detailed in `new_data_processing.R`.