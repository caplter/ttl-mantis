#' @title helper function to confirm that data from input files are reflected
#' in aggregated data to upload
#'
#' @description \code{validate_inputs} is intended to ensure that data from the
#' suite of input files are reflected in the aggregated data that will be
#' uploaded to the database. The check evaluates only that the names of all
#' input files are included in the aggregated data. As such, it should not be
#' construed from this check that all data in the input files are included in
#' the aggregated data but rather that at least one data point from each input
#' file is reflected in the aggregated data.
#'
#' @note As this is used for data validation only, a return value is not
#' specified.
#'
#' @export

validate_inputs <- function(list_of_input_files, aggregated_data) {

  aggregated_data |>
    pointblank::col_vals_in_set(
      columns       = filename,
      set           = basename(list_of_input_files),
      preconditions = function(x) dplyr::distinct(x),
      actions       = pointblank::warn_on_fail(),
      label         = "check that all files in the aggregated data match the input files",
      active        = TRUE
    )

  invisible(
    tibble::tibble(input_files = basename(list_of_input_files)) |>
      pointblank::col_vals_in_set(
        columns       = input_files,
        set           = aggregated_data |> dplyr::distinct(filename) |> dplyr::pull(filename),
        actions       = pointblank::warn_on_fail(),
        label         = "check that all input files match files in the aggregated data",
        active        = TRUE
      )
  )

}
