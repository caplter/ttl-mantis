#! /bin/bash

file=$1

# extract header line to variable (header);
# remove non-data lines to variable (data)
header=$(awk '/Date|DATE/ { myvar = $0 ; print myvar ; exit }' "${file}")
data=$(awk 'BEGIN{FS=","} $3 != "" && !/Eureka|DATE/' "${file}")

# get file basename
filename=$(basename "$file")
fname="${filename%.*}"

# append header and data, output to file with cleaned extension
echo -e "${header}\n${data}" > "${fname}_cleaned.csv"
