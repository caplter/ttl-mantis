#' @title add new readings to the ttl database
#'
#' @description The `insert_new_readings` function adds new sonde readings
#' data, paired to events, to the ttl database by inserting them from the
#' temp_readings table created earlier in the data-processing workflow. The
#' workflow in the function is wrapped in a transation, so changes are commited
#' if successful and rolled back if not. A final, cleaning step removed the
#' temp_readings table from the database as it is not required after this step.
#'
#' @export

insert_new_readings <- function(db_pipe = pg) {

  alter_readings_query <- 'ALTER TABLE ttl_mantis.temp_readings ALTER COLUMN datetime TYPE TIMESTAMP WITHOUT TIME ZONE ;'

  insert_readings_query <- "
  INSERT INTO ttl_mantis.readings
  (
    event_id,
    metric_id,
    value
  )
  (
    SELECT
      events.id,
      metrics.id,
      temp_readings.value
    FROM ttl_mantis.temp_readings
    JOIN ttl_mantis.metrics ON (metrics.metric = temp_readings.metric)
    JOIN ttl_mantis.events ON (events.datetime = temp_readings.datetime)
    )
  ;
  "

  DBI::dbExecute(
    conn      = db_pipe,
    statement = alter_readings_query
  )

  # begin transaction

  DBI::dbBegin(db_pipe)

  n_inserted <- DBI::dbExecute(
    conn      = db_pipe,
    statement = insert_readings_query
  )

  if (n_inserted == nrow(temp_readings)) {

    DBI::dbRemoveTable(
      conn = db_pipe,
      name = c("ttl_mantis", "temp_readings")
    )
    DBI::dbCommit(conn = db_pipe)
    message("inserted ", nrow(temp_readings), " rows")

  } else {

    DBI::dbRollback(conn = db_pipe)
    message("rolling back: ", "target insert => ", n_inserted, " table rows => ", nrow(temp_readings))

    # end transaction

  }

}
